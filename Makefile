
.DEFAULT_GOAL := build
.PHONY: build lint clean

build: pipenv

pipenv:
		pipenv install --dev

lint:
		pipenv run reuse lint

clean: clean.pipenv

clean.pipenv:
		pipenv --rm
